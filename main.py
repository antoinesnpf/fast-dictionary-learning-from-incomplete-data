import numpy as np
import matplotlib.pyplot as plt
from algos.OMP import OMP
from algos.ITK import ITKrM, ITKrMM
from data import generate_synthetic_data, generate_mask_burst, generate_mask_erasure



if __name__ == '__main__':

    # Test with one signal
    if False:
        np.random.seed(12)
        N = 10
        d = 200
        drop_proba = 0.1
        masks = (np.random.random(size=(d, N)) > drop_proba).astype('float')

        w = np.random.uniform(10e-2, 10, size=(N,))
        phase = np.random.uniform(0, 2 * np.pi, size=(N,))

        x = np.linspace(0, 2 * np.pi, d)
        data = np.zeros((d, N))

        for i in range(N):
            data[:, i] = np.diag(masks[:, i]) @ np.sin(w[i] * x + phase[i])

        K = 10
        S = 100
        L = 2

        n_iter_low_rank = 5
        n_iter = 40

        # ITKrMM
        model = ITKrMM(data, masks, K, S, L)
        dico_low_rank = model.fit_predict_low_rank(n_iter_low_rank)
        dico = model.fit_predict(n_iter)
        big_dico = np.concatenate([dico_low_rank, dico], axis=1)

        sparse_model = OMP(big_dico)
        signal = data.T[0]
        x = sparse_model.fit_predict(signal, normalized_coherence_threshold=0.01)

        reconstruct = big_dico @ x
        plt.plot(signal, label="signal")
        plt.plot(reconstruct, label="reconstruction")
        plt.title('ITKrMM + OMP')
        plt.legend()
        plt.show()

        # ITKrM
        model = ITKrM(data, K, S)
        big_dico = model.fit_predict(n_iter)

        sparse_model = OMP(big_dico)
        signal = data.T[0]
        x = sparse_model.fit_predict(signal, normalized_coherence_threshold=0.01)

        reconstruct = big_dico @ x
        plt.plot(signal, label="signal")
        plt.plot(reconstruct, label="reconstruction")
        plt.title('ITKrM + OMP')
        plt.legend()
        plt.show()

    if True:
        np.random.seed(123)

        mask_type = 'erasure'
        # mask_type = 'burst'
        runs = 3
        threshold = 0.75

        N = 1000  # nb of signals used for ITKrM(M)
        d = 100  # dimension of signals

        n_iter_low_rank = 50  # nb of iterations to recover the low-rank component
        n_iter = 100  # nb of iterations for ITKrM(M)
        rho = 0.25 / np.sqrt(d)  # noise level

        K = int(1.5 * d)

        param_lr = {'L': 2, 'bL': 0.15, 'rad': 1 / 3}
        # bL : decay parameter over lr coefficients
        # rad: radius of sphere where lr vectors are generated

        param_sp = {'S': 6, 'b': 0.1, 'sm': 4.}
        # b: decay parameter over sparse coefficients
        # sm : scaling parameter

        if mask_type == 'erasure':
            param_mask = {'p1': 0.33, 'p2': 0.33, 'q1': 0.7, 'q2': 0.5}
            # p1: for each time step in the upper half, probability to be corrupted
            # p2: for each time step in the lower half, probability to be corrupted
            # q1: for each signal, probability to be corrupted
            # q2: for each signal, probability to be corrupted

            # SETTINGS:
            # 10% corruption : p1=p2=0.17, q1=0.7, q2=0.5
            # 20% corruption : p1=p2=0.33, q1=0.7, q2=0.5
            # 30% corruption : p1=p2=0.50, q1=0.7, q2=0.5
            # 40% corruption : p1=p2=0.66, q1=0.7, q2=0.5
            # 50% corruption : p1=p2=0.63, q1=0.9, q2=0.7


        elif mask_type == 'burst':
            # please be sure 2*tau <d
            param_mask = {'p0': 0., 'p1': 0.7, 'p2': 0.3, 'tau': d // 4, 'q': 0.5}
            # p0: for each signal, probability to be clean
            # p1: for each signal, probability of damage of length tau
            # p2: for each signal, probability of damage of length 2*tau
            # tau: length of damage
            # q: for each signal, probability of corruption in the upper half

            # SETTINGS:
            # 10% corruption : p1 = 0.35, p2=0.2, p0=0.45
            # 20% corruption : p1 = 0.5, p2=0.3, p0=0.2
            # 30% corruption : p1 = 0.52, p2=0.4, p0=0.08
            # 40% corruption : p1 = 0.6, p2=0.4, p0=0.
            # 50% corruption : p1 = 0.1, p2=0.9, p0=0.

        # creating the dictionary: dico of size d x (L+K)
        dico = np.random.randn(d, K + param_lr['L'])

        # 1. Low-rank component: normalisation + SVD
        lrc = dico[:, :param_lr['L']]
        scale = np.sqrt(np.sum(lrc ** 2, axis=0))
        lrc = lrc / scale
        u_lrc, _, v_lrc = np.linalg.svd(lrc, full_matrices=False)
        # u_lrc : d x K, v_lrc: K x K
        lrconb = u_lrc @ v_lrc

        # 2. Dictionary with sparsity : orthogonal projection + normalisation
        dico = dico - lrconb @ (lrconb.T @ dico)
        dico[:, :param_lr['L']] = lrconb
        scale = np.sqrt(np.sum(dico ** 2, axis=0))
        dico = dico / scale

        # generating dictionaries for initialisation
        dinit = np.random.randn(d, runs * (K + param_lr['L']))
        dinit /= np.sqrt(np.sum(dinit ** 2, axis=0))
        dinit_lr = dinit[:, :runs * param_lr['L']]
        dinit = dinit[:, runs * param_lr['L']:]

        all_corr = np.zeros(runs)  # empirical average corruption
        # itkrmm
        result_dict = np.zeros(runs)
        result_lr = np.zeros(runs)
        # itkrm
        result_lrb = np.zeros(runs)
        result_dictb = np.zeros(runs)

        for run in range(runs):
            print("Run {}/{}".format(run + 1, runs))
            # generating data
            data = generate_synthetic_data(dico, N, param_sp, param_lr, rho)
            if mask_type == 'erasure':
                masks = generate_mask_erasure(d, N, param_mask)
            elif mask_type == 'burst':
                masks = generate_mask_burst(d, N, param_mask)
            data = data * masks

            # for j in range(3):
            #     plt.plot(data[:, j])
            # plt.show()

            SigCorr = 1. - np.mean(masks)
            all_corr[run] = SigCorr

            model = ITKrMM(data, masks, K, param_sp['S'], param_lr['L'],
                           dic_init=dinit[:, run * K:(run + 1) * K],
                           low_rank_dic_init=dinit_lr[:, run * param_lr['L']:(run + 1) * param_lr['L']])

            modelb = ITKrM(data, K, param_sp['S'], dic_init=dinit[:, run * K:(run + 1) * K])

            # 1. Low Rank: ITKrMM
            print("Low rank approximation: ITKrMM")
            dico_low_rank = model.fit_predict_low_rank(n_iter_low_rank)
            # calculating the error on low-rank
            errLRS = np.linalg.norm(dico[:, :param_lr['L']] - dico_low_rank @ dico_low_rank.T @ dico[:, :param_lr['L']])
            result_lr[run] = errLRS
            print("res:", errLRS)

            # 1. Low Rank: ITKrM
            print("Low rank approximation: ITKrM")
            eigenvals, eigenvectors_right = np.linalg.eigh(data @ data.T)
            idx = eigenvals.argsort()[::-1][:param_lr['L']]
            dico_low_rankb = eigenvectors_right[:, idx]
            errLRSb = np.linalg.norm(
                dico[:, :param_lr['L']] - dico_low_rankb @ dico_low_rankb.T @ dico[:, :param_lr['L']])
            result_lrb[run] = errLRSb
            print("res:", errLRSb)

            # initialisation of dictionaries before learning (orthogonal to low-rank+ normalisation)
            model.dic -= dico_low_rank @ dico_low_rank.T @ model.dic
            model.dic /= np.sqrt(np.sum(model.dic ** 2, axis=0))

            modelb.dic -= dico_low_rankb @ dico_low_rankb.T @ modelb.dic
            modelb.dic /= np.sqrt(np.sum(modelb.dic ** 2, axis=0))

            # 2. Dictionary learning: ITKrMM
            print("Dictionary learning: ITKrMM")
            dico_learning = model.fit_predict(n_iter)

            all_perf = np.max(np.abs(dico_learning.T @ dico[:, param_lr['L']:]), axis=1)
            # print(all_perf)
            res = np.sum(all_perf > threshold) / K
            result_dict[run] = res
            print("res:", res)

            print("Dictionary learning: ITKrM")
            dico_learningb = modelb.fit_predict(n_iter)

            all_perfb = np.max(np.abs(dico_learningb.T @ dico[:, param_lr['L']:]), axis=1)
            # print(all_perfb)
            resb = np.sum(all_perfb > threshold) / K
            result_dictb[run] = resb
            print("res:", resb)
        # end
        print("====SUMMARY==============")
        print("Corruption rate", all_corr)
        print("=========================")
        print("ITKrM result, Low-rank:", result_lrb)
        print("ITKrM result, Low-rank (mean):", np.mean(result_lrb))
        print("ITKrM result, Dictionary:", result_dictb)
        print("ITKrM result, Dictionary (mean):", np.mean(result_dictb))
        print("=========================")
        print("ITKrMM result, Low-rank:", result_lr)
        print("ITKrMM result, Low-rank (mean):", np.mean(result_lr))
        print("ITKrMM result, Dictionary:", result_dict)
        print("ITKrMM result, Dictionary (mean):", np.mean(result_dict))
