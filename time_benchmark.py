from data import masked_sinusoids
from algos.ITK import ITKrM, ITKrMM
from BPFA import BPFA
import matplotlib.pyplot as plt
import time

if __name__ == '__main__':
    ## time benchmark ##

    les_N = [100, 200, 500, 750, 1000]
    d = 200
    K = 30
    S = 10
    L = 2

    n_iter_low_rank = 10
    n_iter = 50
    REPEATS = 3
    
    les_t_IRKrM = []
    les_t_IRKrMM = []
    les_t_BPFA = []

    for N in les_N : 
        data, masks = masked_sinusoids(N, d, drop_proba= 0.4)
  
        #testing ITKrM


        t = time.time()
        for _ in range(REPEATS) : 
            model = ITKrM(data, K, S)
            model.fit_predict(n_iter)
        les_t_IRKrM.append(time.time() - t)


        #testing ITKrMM 
        t = time.time()
        for _ in range(REPEATS) : 
            model_m = ITKrMM(data, masks, K, S, L)
            model_m.fit_predict_low_rank(n_iter_low_rank)
            model_m.fit_predict(n_iter)
        les_t_IRKrMM.append(time.time() - t)

        #testing BPFA
        t = time.time()
        for _ in range(REPEATS) : 
            BPFA(data, masks, K = K, iter = n_iter)
        les_t_BPFA.append(time.time() - t)



    plt.figure()
    plt.title('time benchmark ')
    plt.plot(les_N, les_t_IRKrM, label = 'BPFA')
    plt.plot(les_N, les_t_IRKrMM, label = 'ITKrM')
    plt.plot(les_N, les_t_BPFA, label = 'ITKrMM')
    plt.xlabel('N')
    plt.ylabel('time (s)')
    plt.legend()
    plt.show()
    plt.savefig('time_benchmark.pdf')