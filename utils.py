import numpy as np



def sub_array_by_columns(A, I) : 
    d, N = A.shape
    S = len(I)

    res = np.zeros((d, S))
    for i, idx in enumerate(I) : 
        res[:, i] = A[:, idx]

    return res


def cosine_atom(k, L) : 
    u = np.arange(2*L)
    w = np.sin(np.pi/(2*L)*(u + 0.5))
    return w * np.sqrt(2/L) * np.cos(np.pi/L * (u + (L + 1)/2) * (k + 0.5))

def padded_atom(atom, pad) : 
    assert len(atom) + pad <= 2048, f'too long : {len(atom) + pad}'
    res = np.zeros((2048, )) 
    res[pad: pad + len(atom)] = atom
    return res

def generate_dictionnary():
    dictionnary = []
    for L in [32, 64, 128, 256, 512, 1024] :
        for k in range(L) : 
            for pad in range(0, 2048, L) : 
                if pad + 2 * L <= 2048 : 
                    atom = padded_atom(cosine_atom(k, L), pad)
                    dictionnary.append(atom)    
    return np.array(dictionnary)


def double_sort(A, B, reverse = False) : 
    A, B = zip(*sorted(zip(A, B), reverse = reverse))
    return A, B