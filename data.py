import numpy as np



def masked_sinusoids(d, N, drop_proba) : 
    masks = (np.random.random(size=(d, N)) > drop_proba).astype('float')

    w = np.random.uniform(10e-2, 10, size=(N,))
    phase = np.random.uniform(0, 2 * np.pi, size=(N,))

    x = np.linspace(0, 2 * np.pi, d)
    data = np.zeros((d, N))

    for i in range(N):
        data[:, i] = np.diag(masks[:, i]) @ np.sin(w[i] * x + phase[i])

    return data, masks


def generate_synthetic_data(dico, N, param_sp, param_lr, rho):
    """

    :param dico: generating dictionary (first columns: low-rank, then: sparse dictionary)
    :param N: number of training signals
    :param param_sp: dict for sparsity info
    :param param_lr: dict for low-rank info
    :param rho: noise level
    :return: d x N matrix = training signals
    """

    S, b, sm = param_sp['S'], param_sp['b'], param_sp['sm']
    L, bL, rad = param_lr['L'], param_lr['bL'], param_lr['rad']
    d, K_all = dico.shape

    signals = np.zeros((d, N))
    for n in range(N):
        beta = b * np.random.rand() + 1. - b
        betaL = bL * np.random.rand() + 1. - bL

        # low-rank component
        x_1_L = betaL ** np.arange(1, L + 1)
        x_1_L_sign = 2 * np.random.rand(L) - 1.
        x_1_L *= x_1_L_sign
        x_1_L /= np.sqrt(np.sum(x_1_L ** 2))
        x_1_L *= rad
        sig = dico[:, :L] @ x_1_L

        # sparse dictionary
        x_L_S = beta ** np.arange(1, S + 1)
        x_L_S_sign = 2 * np.random.rand(S) - 1.
        x_L_S *= x_L_S_sign
        x_L_S /= np.sqrt(np.sum(x_L_S ** 2))
        x_L_S *= np.sqrt(1 - rad ** 2)

        indices_sparse = L + np.random.choice(K_all - L, S)
        sig += dico[:, indices_sparse] @ x_L_S

        # noise
        noise = rho * np.random.randn(d)
        sig += noise
        sig /= np.sqrt(1 + np.sum(noise ** 2))

        # scale
        scale = 1. + np.random.rand() * (sm - 1.)
        sig *= scale

        signals[:, n] = sig

    return signals


def generate_mask_erasure(d, N, param_mask):
    """

    :param d: signal dimension
    :param N: nb of signals
    :param param_mask: dict for mask info
    :return: d x N matrix = mask coefficients
    """
    # probability across the signal
    qprob = np.random.rand(N)
    temp = qprob <= 0.5
    qprob[temp] = param_mask['q1']
    qprob[np.invert(temp)] = param_mask['q2']
    qprob = qprob.reshape(1, -1)

    pprob = np.zeros(d)
    pprob[:np.ceil(d / 2).astype(int)] = param_mask['p1']
    pprob[np.ceil(d / 2).astype(int):] = param_mask['p2']
    pprob = pprob.reshape(-1, 1)

    mask = np.random.rand(d, N)
    mask_prob = pprob @ qprob
    mask = np.ceil(mask - mask_prob)

    return mask


def generate_mask_burst(d, N, param_mask):
    """

    :param d: signal dimension
    :param N: nb of signals
    :param param_mask: dict for mask info
    :return: d x N matrix = mask coefficients
    """
    mask = np.ones((d, N))

    # probability across the signal
    qprob = np.random.rand(N)
    qprob[qprob <= param_mask['p0']] = 0.
    qprob[qprob > (param_mask['p1'] + param_mask['p0'])] = 2.  # damage of length 2*tau
    qprob[(qprob < (param_mask['p1'] + param_mask['p0'])) * (qprob > param_mask['p0'])] = 1.  # damage of length tau

    # Start Location of the damage, randomly generated
    qprobpos = np.random.rand(N)
    qprobpos[qprobpos > param_mask['q']] = 0.
    qprobpos = np.ceil(qprobpos).astype(int)

    qpos = np.ones(N)
    size_1 = np.sum(qprobpos == 1)
    qpos[qprobpos == 1] = np.random.randint(low=0, high=np.ceil(d / 2).astype(int), size=size_1)
    qpos[qprobpos == 0] = np.random.randint(low=np.ceil(d / 2).astype(int), high=d, size=N - size_1)
    qpos[qprob == 0.] = 0

    # damage of length tau
    tt = qpos[qprob == 1]
    index_tt = np.where(qprob == 1)[0]

    for i in range(tt.size):
        if tt[i] + param_mask['tau'] < d:
            index = np.arange(tt[i], tt[i] + param_mask['tau'])
        else:
            temp = d - tt[i]
            index = np.concatenate((np.arange(0, param_mask['tau'] - temp), np.arange(tt[i], d)))
        index = index.astype(int)
        mask[index, index_tt[i]] = 0

    # damage of length tau
    t2 = qpos[qprob == 2]
    index_t2 = np.where(qprob == 2)[0]

    for i in range(t2.size):
        if t2[i] + 2 * param_mask['tau'] < d:
            index = np.arange(t2[i], t2[i] + 2 * param_mask['tau'])
        else:
            temp = d - t2[i]
            index = np.concatenate((np.arange(0, 2 * param_mask['tau'] - temp), np.arange(t2[i], d)))
        index = index.astype(int)
        mask[index, index_t2[i]] = 0
    return mask