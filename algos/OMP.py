import numpy as np
import copy
import matplotlib.pyplot as plt
from utils import generate_dictionnary, sub_array_by_columns



class OMP:
    
    def __init__(self, dictionnary) : 
        'dictionnary : d X K array of K atoms, the column vectors of dimension d'
        self.dictionnary = dictionnary

    def project_dictionnary(self, index_set) : 
        'returns dictionnary where lines included in index_set are set to 0'
        temp = copy.copy(self.dictionnary.T)
        for i in index_set : 
            temp[i] = 0
        return temp

    def reduce_dictionnary(self, index_set):
        res = []
        for i in index_set : 
            res.append(self.dictionnary.T[i])
        return np.array(res)

    def fit_predict(self, signal, normalized_coherence_threshold = 0.2) : 

        self.normalized_coherences = []
        self.erros = []

        T = len(signal)
        residu = copy.copy(signal)
        reconstruction = np.zeros((T, ))
        x = np.zeros((len(self.dictionnary.T), ))

        index_set = []
        self.normalized_coherence = np.inf
        self.mse = np.inf

        while self.normalized_coherence > normalized_coherence_threshold:  

            #step 1
            projected_dictionnary = self.project_dictionnary(index_set)
            correls =  projected_dictionnary @ residu
            i_max = np.argmax(np.abs(correls))
            index_set.append(i_max)

            #step 2
            rd = self.reduce_dictionnary(index_set)
            x_reduced = np.linalg.pinv(rd).T @ signal
            for i, j in enumerate(index_set) : 
                x[j] = x_reduced[i]
            
            # step 3
            reconstruction = self.dictionnary @ x
            residu = signal - reconstruction

            #computing coherence
            self.normalized_coherence = abs(correls[i_max]) / np.linalg.norm(residu)
            self.mse = np.linalg.norm(residu)

        return x



class OMPm:

    def __init__(self, dictionnary):
        'dictionnary : d X K array of K atoms, the column vectors of dimension d'
        self.dictionnary = dictionnary

    def fit_predict(self, My, M, S) :
        """
            dic : d x K array, dictionnary with atoms as its columns
            My: array of size (d, ), one signal of corrupted data
            M: array of size (d, ), mask for the corrupted signal
            S: int, desired sparsity

            returns x : sparse vector decomposing y in dic, i.e. such that dic @ x ~= y 
        """

        thresh = 1e-3
        dic = self.dictionnary

        d, K = dic.shape
        M = np.diag(M)
        masked_dic = M @ dic

        r = copy.copy(My) # residual 
        x = np.zeros((K, ))
        I = []
        while len(I) < S and np.linalg.norm(r) > thresh: 

            #find most correlated atom
            num = masked_dic.T @ r
            denum = np.linalg.norm(masked_dic, axis = 0)
            target = np.abs(num) / denum
            j = np.argmax(target)

            I.append(j)
            sub_dic = sub_array_by_columns(masked_dic, I)
            x[I] = np.linalg.pinv(sub_dic) @ My 
            r = My - sub_dic @ x[I]

        return x



if __name__ == '__main__': 

    dictionnary = generate_dictionnary().T
    signal = np.sin(np.arange(len(dictionnary)) / 100)
    mask = np.ones(len(dictionnary))
    sparsity = 10


    if True : 
        sparse_model = OMP(dictionnary)

        x = sparse_model.fit_predict(signal)
        reconstruction = dictionnary @ x

        plt.plot(signal, label = "signal")
        plt.plot(reconstruction, label = "reconstruction")
        plt.title(f'OMP reconstruction - normalized coherence: {sparse_model.normalized_coherence:.2f} MSE: {sparse_model.mse :.2f}')
        plt.legend()
        plt.show()


    if True : 

        sparse_model = OMPm(dictionnary)

        x = sparse_model.fit_predict(signal, mask, S = sparsity)
        reconstruction = dictionnary @ x

        plt.plot(signal, label = "signal")
        plt.plot(reconstruction, label = "reconstruction")
        plt.title(f'OMPm reconstruction, sparisity = {sparsity}')
        plt.legend()
        plt.show()




