import numpy as np
from utils import double_sort
from tqdm import tqdm
import time
from data import masked_sinusoids

def P(A):
    return A @ np.linalg.pinv(A)

def Q(A):
    n, p = A.shape
    return np.eye(n) - P(A)


def sub_array_by_columns(A, I):
    d, N = A.shape
    S = len(I)

    res = np.zeros((d, S))
    for i, idx in enumerate(I):
        res[:, i] = A[:, idx]

    return res


class ITKrM:

    def __init__(self, data, K, S, dic_init=None):
        """args : 
            data : d x N array containing the signals as its columns
            K : dictionnary size 
            S : sparisty of the signals
            dic_init : d x K array, initial dictionnary
        """

        self.d, self.N = data.shape
        self.data = data
        self.K = K
        self.S = S
        if dic_init is None:
            dic_init = np.random.normal(size=(self.d, K))
        assert dic_init.shape == (self.d, self.K)
        self.dic = dic_init

        self.t1 = 0
        self.t2 = 0

    def step(self):
        dic = self.dic

        # step 1
        t = time.time()
        I_list = []
        for n in range(self.N):
            y = self.data[:, n]
            target = np.abs(dic.T @ y)
            indices = np.arange(self.K)
            target, indices = double_sort(target, indices, reverse=True)
            I_list.append(indices[0:self.S])
        self.t1 += time.time() - t
        

        # step 2
        t = time.time()
        psi_list = []
        for k in range(self.K):


            psi = dic[:, k]
            psi_new = np.zeros_like(psi)

            for n in range(self.N):
                y = self.data[:, n]
                
                sub_dic = sub_array_by_columns(dic, I_list[n])

                if k in I_list[n]:
                    psi_new += (np.eye(self.d) - P(sub_dic) + P(psi[:, None])) @ y * np.sign(psi @ y)

            norm =  np.linalg.norm(psi_new)
            if norm == 0 : 
                norm = 1 
            psi_list.append(psi_new / norm)


        dic = np.array(psi_list).T
        self.dic = dic
        self.t2 += time.time() - t

    def fit_predict(self, n_iter=50):
        for _ in tqdm(range(n_iter)):
            self.step()
        return self.dic


class ITKrMM:

    def __init__(self, data, masks, K, S, L, dic_init=None, low_rank_dic_init=None):
        """args : 
            data : d x N array containing the CORRUPTED signals as its columns
            masks : d x M array containin the masks as its columns
            K : dictionnary size 
            S : sparisty of the signals
            dic_init : d x K array, initial dictionnary
        """
        self.d, self.N = data.shape
        self.data = data
        self.masks = masks
        self.K = K
        self.S = S
        self.L = L

        if dic_init is None:
            dic_init = np.random.normal(size=(self.d, K))
        assert dic_init.shape == (self.d, self.K)
        self.dic = dic_init

        if low_rank_dic_init is None:
            low_rank_dic_init = np.random.normal(size=(self.d, L))
        assert low_rank_dic_init.shape == (self.d, self.L)
        self.low_rank_dic = low_rank_dic_init

        self.t1 = 0
        self.t2 = 0
        self.t_lr = 0

    def step(self):
        self.dic

        # step 1
        t = time.time()
        My__list = []
        I_list = []
        for n in range(self.N):
            My = self.data[:, n]
            M = np.diag(self.masks[:, n])
            My_ = Q(M @ self.low_rank_dic) @ My
            My__list.append(My_)

            masked_dic = M @ self.dic
            num = masked_dic.T @ My_
            denum = np.linalg.norm(masked_dic, axis=0)

            target = np.abs(num) / denum
            indices = np.arange(self.K)
            target, indices = double_sort(target, indices, reverse=True)

            I_list.append(indices[0:self.S])
        self.t1 += time.time() - t

        # step 2 
        t = time.time()
        psi_list = []
        for k in range(self.K):

                        
            psi = self.dic[:, k]
            psi_new = np.zeros_like(psi)
            W = np.zeros((self.d, self.d))

            for n in range(self.N):
                if k in I_list[n]:
                    My = self.data[:, n]
                    My_ = My__list[n]
                    sub_dic = sub_array_by_columns(self.dic, I_list[n])
                    cat = np.concatenate([self.low_rank_dic, sub_dic], axis=1)
                    M = np.diag(self.masks[:, n])

                    psi_new += (np.eye(self.d) - P(M @ cat) + P((M @ psi)[:, None])) @ My_ * np.sign(psi @ My_)
                    W += M

            psi_new = Q(self.low_rank_dic) @ np.linalg.pinv(W) @ psi_new
            norm = np.linalg.norm(psi_new)
            if norm == 0 :
                norm = 1
            psi_list.append(psi_new / norm)

        dic = np.array(psi_list).T
        print(dic.shape)
        self.dic = dic

        self.t2 += time.time() - t

    def low_rank_step(self):
        t = time.time()
        gamma_list = []
        for l in range(self.L):

            tilde_Gamma = self.low_rank_dic[:, :l]
            My__list = []
            for n in range(self.N):
                My = self.data[:, n]
                M = np.diag(self.masks[:, n])
                My_ = Q(M @ tilde_Gamma) @ My
                My__list.append(My_)


            gamma = self.low_rank_dic[:, l]
            new_gamma = np.zeros_like(gamma)
            
            W = np.zeros((self.d, self.d))
            for n in range(self.N):
                My_ = My__list[n]
                M = np.diag(self.masks[:, n])
                cat = np.concatenate([tilde_Gamma, gamma[:, None]], axis=1)

                new_gamma += (np.eye(self.d) - P(M @ cat) + P((M @ gamma)[:, None])) @ My_ * np.sign(gamma @ My_)
                W += M

            new_gamma = Q(tilde_Gamma) @ np.linalg.pinv(W) @ new_gamma
            norm = np.linalg.norm(new_gamma)
            if norm == 0 :
                norm = 1
            new_gamma = new_gamma / norm
            self.low_rank_dic[:, l] = new_gamma
            gamma_list.append(new_gamma)

        self.t_lr += time.time() - t

    def fit_predict(self, n_iter=50):
        for _ in tqdm(range(n_iter)):
            self.step()
        return self.dic

    def fit_predict_low_rank(self, n_iter=50):
        for _ in tqdm(range(n_iter)):
            self.low_rank_step()
        return self.low_rank_dic


if __name__ == '__main__' : 
    N = 300
    d = 200
    K = 199
    S = 6
    L = 2

    n_iter_low_rank = 10
    n_iter = 50
    REPEATS = 3
    
    les_t_IRKrM = []
    les_t_IRKrMM = []
    les_t_BPFA = []

    data, masks = masked_sinusoids(d, N, drop_proba= 0.4)

    #testing ITKrM


    t = time.time()
    for _ in range(REPEATS) : 
        model = ITKrM(data, K, S)
        model.fit_predict(n_iter)
    les_t_IRKrM.append(time.time() - t)


    #testing ITKrMM 
    t = time.time()
    for _ in range(REPEATS) : 
        model_m = ITKrMM(data, masks, K, S, L)
        model_m.fit_predict_low_rank(n_iter_low_rank)
        model_m.fit_predict(n_iter)
    les_t_IRKrMM.append(time.time() - t)



