#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from os.path import join, dirname
import numpy as np
import scipy.io as sio
import time
import warnings
import subprocess
import os
import matplotlib.pyplot as plt
from data import masked_sinusoids
from cfg import BPFA_PATH

# code adapted from https://github.com/etienne-monier/BPFA


def run(cmd) : 
    p = subprocess.Popen(cmd,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)

    for line in iter(p.stdout.readline, b''):
        print(">>> " + line.rstrip().decode())



def BPFA(Y, mask, path = BPFA_PATH, **kwargs):
    """
    Implements the BPFA code from python to matlab.

    Args:
        Y (2D or 3D Numpy array): The input data.
        mask (2D Numpy array): The acquisition mask.

    Keyword Args:
        PatchSize (int): The patch size. Default is 5.
        Omega (int): The Omega parameter. Default is 1.
        K (int): The dictionary dimension. Default is 128.
        iter (int): The number of iterations. Default is 100.
        step (int): The distance between two consecutive patches. Default is 1 for full overlap.
        verbose (bool): The verbose parameter. Default is True.

    Returns:
        Xhat (2D or 3D Numpy array): The reconstructed image.
        InfoOut (dictionary): Dictionary containing the following informations: 'dt' which is the elapsed time.
        
    """

    ##
    ## Catch keyword parameters.
    ##

    # Default values
    PatchSize = 5
    Omega = 1
    K = 128
    Niter = 50
    step = 1
    verbose = True

    # Catch input
    for key, value in kwargs.items():

        if key.upper() == 'PATCHSIZE':
            if type(value) is not int:
                raise ValueError('The patch size parameter is not an integer.')
            if value <= 0 or value > np.min(Y.shape[:2]):
                raise ValueError('The patch size should lay between 1 and {}.'.format(np.min(Y.shape[:2])))
            PatchSize = value

        elif key.upper() == 'OMEGA':
            if type(value) is not int and type(value) is not float:
                raise ValueError('The Omega parameter is not an integer nor a float.')
            if value <= 0:
                raise ValueError('The Omega should be positive.')
            Omega = value

        elif key.upper() == 'K':
            if type(value) is not int:
                raise ValueError('The K parameter is not an integer.')
            if value <= 0:
                raise ValueError('The K should be positive.')
            K = value

        elif key.upper() == 'ITER':
            if type(value) is not int:
                raise ValueError('The iter parameter is not an integer.')
            if value <= 0:
                raise ValueError('The iter should be positive.')
            Niter = value

        elif key.upper() == 'STEP':
            if type(value) is not int:
                raise ValueError('The step parameter is not an integer.')
            if value <= 0:
                raise ValueError('The step should be positive.')
            step = value

        elif key.upper() == 'VERBOSE':
            if type(value) is not bool:
                raise ValueError('The verbose parameter is not boolean.')
            verbose = value

        else:
            warnings.warn('Unknown keyword argument: {}.'.format(key))


    # Sets the data name files.
    dateStr = time.strftime('%A-%d-%B-%Y-%Hh%Mm%Ss', time.localtime())
    code = id(dateStr)
    inName = join(path, 'InOut', f'BPFA_in_{code}.mat')
    outName = join(path, 'InOut', f'BPFA_out_{code}.mat')

    #Execute program.

    # Arguments.
    Dico = {'Y': Y,
            'mask': mask,
            'PatchSize': PatchSize,
            'Omega': Omega,
            'K': K,
            'iter': Niter,
            'Step': step,
            'outName': outName,
            'verbose': verbose}

    # Save temp data
    sio.savemat(inName, Dico)

    # Execute program.
    matlab_inName = inName.replace('\\', '/')
    cwd = path.replace('\\', '/')
    cmd = [
    "C:\\Program Files\\MATLAB\\R2022a\\bin\\matlab", 
    "-nodesktop", 
    "-nosplash", 
    "-sd", cwd,
    "-r", f"load('{matlab_inName}'); run BPFA_for_python.m; quit"
    ]

    run(cmd)


    while not os.path.isfile(outName) : 
        time.sleep(1)

    # Get output data
    data = sio.loadmat(outName)
    reconstruct = data['Xhat']
    dt = data['time']

    # rm out and in
    os.remove(inName)
    os.remove(outName)

    InfoOut = {'time': dt}

    return reconstruct


if __name__ == '__main__' : 

    N = 100
    d = 200
    K = 40
    S = 15

    drop_proba = 0.8
    data, masks = masked_sinusoids(d, N, drop_proba) 

   
    x = BPFA(data, masks, K = K, iter = 50)

    for i in range(5):
        plt.figure()
        plt.title('BPFA - demo on test signals')
        plt.plot(data[:, i], label = f'true signal {i}')
        plt.plot(x[:, i], label = f'reconstruction {i}')
        plt.legend()
        plt.show()