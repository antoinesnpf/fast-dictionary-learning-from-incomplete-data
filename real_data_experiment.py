import numpy as np
from tqdm import tqdm
from data import generate_mask_erasure, generate_mask_burst
from loadmydata.load_human_locomotion import load_human_locomotion_dataset, get_code_list
from algos.ITK import ITKrM, ITKrMM
from algos.BPFA import BPFA
from algos.OMP import OMPm, OMP
from dtaidistance.dtw  import distance as dtw_dist
import matplotlib.pyplot as plt
import time

np.random.seed(123)



# load real data
signal_trunc_in = 1000
signal_trunc_out = 2000
Nmax = 300
code_list = get_code_list()
X_train = list() 


for code in tqdm(code_list):
    single_trial = load_human_locomotion_dataset(code)
    signal = single_trial.signal.LAZ.to_numpy()[signal_trunc_in:signal_trunc_out]  
    if (len(signal) == (signal_trunc_out - signal_trunc_in)) and len(X_train) < Nmax:    
        X_train.append(signal[::5])  

    
data = np.asarray(X_train).T
d, N = data.shape
K = 220
L = 2
S = 6
LR_ITER = 2
ITER = 2
RUNS = 1
TYPE = ['erasure', 'burst'][0] #pick one

# apply erasure masks




if TYPE == 'erasure' : 
    # p1: for each time step in the upper half, probability to be corrupted
    # p2: for each time step in the lower half, probability to be corrupted
    # q1: for each signal, probability to be corrupted
    # q2: for each signal, probability to be corrupted

    param_masks = [
        {'p1': 0.00, 'p2': 0.00, 'q1': 0.7, 'q2': 0.5}, # 0% corruption
        {'p1': 0.17, 'p2': 0.17, 'q1': 0.7, 'q2': 0.5}, # 10% corruption
        {'p1': 0.33, 'p2': 0.33, 'q1': 0.7, 'q2': 0.5}, # 20% corruption
        {'p1': 0.50, 'p2': 0.50, 'q1': 0.7, 'q2': 0.5}, # 30% corruption
        {'p1': 0.66, 'p2': 0.66, 'q1': 0.7, 'q2': 0.5}, # 40% corruption
        {'p1': 0.63, 'p2': 0.63, 'q1': 0.9, 'q2': 0.7}  # 50% corruption
    ]

elif TYPE == 'burst' : 
    # p0: for each signal, probability to be clean
    # p1: for each signal, probability of damage of length tau
    # p2: for each signal, probability of damage of length 2*tau
    # tau: length of damage
    # q: for each signal, probability of corruption in the upper half

    param_masks = [
        {'p0': 1, 'p1': 0, 'p2': 0, 'tau': d // 4, 'q': 0.5},         # 0% corruption
        {'p0': 0.45, 'p1': 0.35, 'p2': 0.2, 'tau': d // 4, 'q': 0.5}, # 10% corruption
        {'p0': 0.2, 'p1': 0.5, 'p2': 0.3, 'tau': d // 4, 'q': 0.5},   # 20% corruption
        {'p0': 0.08, 'p1': 0.52, 'p2': 0.4, 'tau': d // 4, 'q': 0.5}, # 30% corruption
        {'p0': 0., 'p1': 0.6, 'p2': 0.4, 'tau': d // 4, 'q': 0.5},    # 40% corruption
        {'p0': 0., 'p1': 0.1, 'p2': 0.9, 'tau': d // 4, 'q': 0.5}     # 50% corruption
    ]


corruptions = [0, 10, 20, 30, 40, 50]

perf_mse_a = []
perf_mse_b = []
perf_mse_c = []
perf_dtw_a = []
perf_dtw_b = []
perf_dtw_c = []


print(f'RUN EXPERIENCE >>{TYPE}<< with d={d} ; N={N}; K={K} ; L={L} ; S={S}')

for etape, corruption in enumerate(corruptions) : 

    t = time.time()
    if TYPE == 'erasure' : 
        masks = generate_mask_erasure(d, N, param_masks[etape])
    elif TYPE == 'burst' : 
        masks = generate_mask_burst(d, N, param_masks[etape])

    masked_data = data * masks

    temp_mse_a = []
    temp_mse_b = []
    temp_mse_c = []
    temp_dtw_a = []
    temp_dtw_b = []
    temp_dtw_c = []
    for run in range(RUNS) : 
        t1 = time.time()
        ###ITKrM###
        modela = ITKrMM(masked_data, masks, K, S, L)
        
        # low rank for ITKrMM
        dico_low_rank = modela.fit_predict_low_rank(LR_ITER)

        # init dic for ITKrMM and ITKrM
        modela.dic -= dico_low_rank @ dico_low_rank.T @ modela.dic
        modela.dic /= np.sqrt(np.sum(modela.dic ** 2, axis=0))

        # train ITKrMM
        dico_a = modela.fit_predict(ITER)
        big_dico = np.concatenate([dico_low_rank, dico_a], axis=1)
        sparse_model_a = OMPm(big_dico)
        reconstruct_a = np.zeros((d, N))
        for i, (signal, mask) in enumerate(zip(masked_data.T, masks.T)) : 
            reconstruct_a[:, i] = big_dico @ sparse_model_a.fit_predict(signal, mask, S)
        mse_a = np.linalg.norm(reconstruct_a - data, axis = 0).mean()
        dtw_a = np.array([dtw_dist(reconstruct_a.T[i], data.T[i]) for i in range(len(data.T))]).mean()
        print(f'CORRUPT LEVEL [{etape+1}/{len(corruptions)}] ITKrMM trained in {time.time() - t1:.2f} s')

        ### ITKrM ###
        t2 = time.time()
        modelb = ITKrM(masked_data, K, S)
        dico_b = modelb.fit_predict(ITER)
        sparse_model_b = OMP(dico_b)
        reconstruct_b = np.zeros((d, N))
        for i, (signal, mask) in enumerate(zip(masked_data.T, masks.T)) : 
            reconstruct_b[:, i] = dico_b @ sparse_model_b.fit_predict(signal)
        mse_b = np.linalg.norm(reconstruct_b - data, axis = 0).mean()
        dtw_b = np.array([dtw_dist(reconstruct_b.T[i], data.T[i]) for i in range(len(data.T))]).mean()
        print(f'CORRUPT LEVEL [{etape+1}/{len(corruptions)}] ITKrM trained in {time.time() - t2:.2f} s')

        ### BPFA ###
        t3 = time.time()
        reconstruct_c = BPFA(masked_data, masks, K = K, iter = int(ITER/2))
        mse_c = np.linalg.norm(reconstruct_c - data, axis = 0).mean()
        dtw_c = np.array([dtw_dist(reconstruct_c.T[i], data.T[i]) for i in range(len(data.T))]).mean()
        print(f'CORRUPT LEVEL [{etape+1}/{len(corruptions)}] BPFA trained in {time.time() - t3:.2f} s')

        temp_mse_a.append(mse_a)
        temp_mse_b.append(mse_b)
        temp_mse_c.append(mse_c)
        temp_dtw_a.append(dtw_a)
        temp_dtw_b.append(dtw_b)
        temp_dtw_c.append(dtw_c)


        fig, axs = plt.subplots(nrows = 5, ncols=1)
        fig.set_figheight(10)
        fig.set_figwidth(10)
        axs[0].plot(data[:,0])
        axs[0].set_title('signal')
        axs[1].plot(masked_data[:,0], color = 'orange')
        axs[1].plot(masked_data[:,0], 'o', color = 'orange')
        axs[1].set_title('masked signal')
        axs[2].plot(reconstruct_a[:,0], color = 'magenta')
        axs[2].set_title('ITKrMM reconstruction')
        axs[3].plot(reconstruct_b[:,0], color = 'cyan')
        axs[3].set_title('ITKrM reconstruction')
        axs[4].plot(reconstruct_c[:,0], color = 'green')
        axs[4].set_title('BPFA reconstruction')
        plt.savefig(f'real_data-exp-{TYPE}-etape{etape}-bigplot.pdf')
        plt.show()

    perf_mse_a.append(np.mean(temp_mse_a))
    perf_mse_b.append(np.mean(temp_mse_b))
    perf_mse_c.append(np.mean(temp_mse_c))
    perf_dtw_a.append(np.mean(temp_dtw_a))
    perf_dtw_b.append(np.mean(temp_dtw_b))
    perf_dtw_c.append(np.mean(temp_dtw_c))

    print('==================')
    print(f'CORRUPT LEVEL [{etape+1}/{len(corruptions)}] done in {time.time() - t:.2f} s')

plt.figure()
plt.title(f'Comparing the algorithms on heartbeats with {TYPE}')
plt.xlabel('corrutpion (%)')
plt.ylabel('MSE')
plt.plot(corruptions, perf_mse_a, label = 'ITKrMM')
plt.plot(corruptions, perf_mse_b, label = 'ITKrM')
plt.plot(corruptions, perf_mse_c, label = 'BPFA')
plt.legend()
plt.savefig(f'real_data_exp-{TYPE}-mse.pdf')
plt.show()

plt.figure()
plt.title(f'Comparing the algorithms on heartbeats with {TYPE}')
plt.xlabel('corrutpion (%)')
plt.ylabel('DTW distance')
plt.plot(corruptions, perf_dtw_a, label = 'ITKrMM')
plt.plot(corruptions, perf_dtw_b, label = 'ITKrM')
plt.plot(corruptions, perf_dtw_c, label = 'BPFA')
plt.legend()
plt.savefig(f'real_data_exp-{TYPE}-dtw.pdf')
plt.show()